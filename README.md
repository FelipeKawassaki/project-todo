# README #

### Desafio ###

- Use tudo como se fosse um projeto seu para o mercado, chame um amigo, pegue um componente do github, pesquise no google.
- Não admitimos trapaça, temos certeza que saberá diferenciar o item acima desse.
- Quanto menos código melhor.
- Preferencialmente use a plataforma para a qual se candidatou. Se a intenção é aprender e ainda não conhece, não tem problema, use a plataforma que preferir.
- Não queremos produtos perfeitos, apenas conhecer um pouco como trabalha. Se gastar mais de 4h, então já é mais do que esperávamos tomar do seu tempo.
- Não precisa manter o estado da aplicação, pode fazer tudo em memória


### Escopo ###

- A tela principal deve ser a minha lista de tarefas
- Permitir que o usuário consiga facilmente adicionar uma nova tarefa
- Possibilitar a finalização de uma tarefa e poder desfazer isso
- Facilitar a visualização das atividades pendentes e finalizadas separadamente
- O usuário pode excluir uma tarefa, mas garantir que ele tenha certeza disso

### Execução do Projeto ###

- Projeto criado em Angular 4 e para rodar o projeto o avaliador deverá seguir os seguintes passos:

	- Dependências
		- Clonar o projeto via git nesse repositório
		- Instalar o node.js (Latest) "https://nodejs.org/en/"
		- Instalar o npm builder (Latest)  "https://docs.npmjs.com/cli/install"
		- Por último instalar o @angular/compiler-cli "https://www.npmjs.com/package/@angular/compiler-cli/tutorial"
	
	 - Rodando o Projeto
	    - Navegar até a pasta do projeto;
		- Executar o seguinte comando: ng serve (pega as configurações default)
		- Rodar em porta expecífica ng serve --port <sua_porta>
		- Rodar em ip expecífico ng serve --host <seu_ip>


