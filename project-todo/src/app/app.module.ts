import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';


import {AppComponent} from './app.component';
import {SearchComponent} from './search/search.component';
import {DetailsComponent} from './details/details.component';

import { MaterializeModule } from 'angular2-materialize';


const appRoutes: Routes = [
  {path: 'app-search', component: SearchComponent},
  {path: 'app-details/:id', component: DetailsComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    DetailsComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    MaterializeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
