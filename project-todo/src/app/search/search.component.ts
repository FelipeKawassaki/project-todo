import { DetailsComponent } from '../details/details.component';
import { Component, OnInit, ViewChild, EventEmitter, Pipe } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';

import { FormGroup, FormArray, Validators, NgModel } from '@angular/forms';

import { MaterializeModule } from 'angular2-materialize';
import 'materialize-css';

export class Todo {
  id: string;
  nome: string;
  status: string;
  detalhe: string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  checkPendente: true;
  checkConcluida: true;


  public myForm: FormGroup;

  id: '';
  nome: '';
  status: '';
  detalhe: '';

  todos: Todo[] = [
    { id: '1', nome: 'Teste1', status: 'PENDENTE', detalhe: 'Tarefa de teste 1' },
    { id: '2', nome: 'Teste2', status: 'CONCLUIDA', detalhe: 'Tarefa de teste 2' },
    { id: '3', nome: 'Teste3', status: 'CONCLUIDA', detalhe: 'Tarefa de teste 3' },
    { id: '4', nome: 'Teste4', status: 'PENDENTE', detalhe: 'Tarefa de teste 4' },
  ];

  modalActions = new EventEmitter<string | MaterializeAction>();
  constructor() { }

  confirmDelete(todo) {
    this.modalActions.emit({ action: 'modal', params: ['open'] });
  }
  closeModalDelete() {
    this.modalActions.emit({ action: 'modal', params: ['close'] });
  }

  delete(obj) {
    let index = this.todos.indexOf(obj);
    this.todos.splice(index, 1);
    this.closeModalDelete();
  }

  reopen(id) {
    let isReopen = this.todos.find(todos => todos.id === id);
    if (isReopen) {
      isReopen.status = 'PENDENTE';
    }
  }
  finish(id) {
    let isFinish = this.todos.find(todos => todos.id === id);
    if (isFinish) {
      isFinish.status = 'CONCLUIDA';
    }
  }

  cadastrar() {
    let cadastro: Todo;
    cadastro = Object.create(Todo.prototype);

    cadastro.id = this.id;
    cadastro.nome = this.nome;
    cadastro.status = 'PENDENTE';
    cadastro.detalhe = this.detalhe;

    this.todos.push(cadastro);
    this.limpaCampos();
    console.log(cadastro);
  }

  limpaCampos() {
    this.id = '';
    this.nome = '';
    this.status = '';
    this.detalhe = '';
  }

  onSubmit(form: any): void {
    console.log('you submitted value:', form);
  }

  ngOnInit() {

  }
}


